package com.tlabs.eve.proxy;




import org.apache.camel.Exchange;
import org.apache.camel.RuntimeCamelException;
import org.apache.camel.component.cache.CacheConstants;
import org.apache.camel.component.http.HttpEndpoint;
import org.apache.camel.component.http.HttpMessage;
import org.apache.camel.spi.HeaderFilterStrategy;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;


public class EveProxyBinding extends FixedHttpBinding {

	public EveProxyBinding(HttpEndpoint endpoint) {
		super(endpoint);
	}

	public EveProxyBinding() {
		super();
	}

	public EveProxyBinding(HeaderFilterStrategy headerFilterStrategy) {
		super(headerFilterStrategy);
	}
	
	@Override
	public void readRequest(HttpServletRequest request, HttpMessage message) {
		super.readRequest(request, message);
		message.getExchange().setProperty(Exchange.HTTP_METHOD, request.getMethod());
		String key = "";
		if (request.getMethod().equals("POST")) {
			key = readPOSTRequestKey(request, message);
			message.setHeader(Exchange.HTTP_QUERY, key);
		}
		else if (request.getMethod().equals("GET")) {
			key = readGETRequestKey(request, message);			
		}		
		else {
			throw new RuntimeCamelException("Unsupported HTTP method " + request.getMethod());
		}
		
		message.setHeader(CacheConstants.CACHE_KEY, md5(request.getRequestURI() + key));		
	}
	
	private String readPOSTRequestKey(HttpServletRequest request, HttpMessage message) {
		String key = "";
		
		String body = message.getBody(String.class);
		if (StringUtils.isBlank(body)) {
			return key;
		}
		String charset = request.getCharacterEncoding();
		if (charset == null) {
			charset = "UTF-8";
		}
		try {
			boolean first = true;
			for (String param : body.split("&")) {
				String[] pair = param.split("=", 2);
				if (pair.length == 2) {
					String name = URLDecoder.decode(pair[0], charset);
					String value = URLDecoder.decode(pair[1], charset);
					key = key + (first ? "?" : "&") + name + "=" + value;
					first = false;
				}					
				else {
					throw new IllegalArgumentException("Invalid parameter, expected to be a pair but was " + param);
				}
			}		
		}
		catch (UnsupportedEncodingException e) {
			throw new RuntimeCamelException("Cannot read request parameters due " + e.getMessage(), e);
		}
		return key;
	}
	
	private String readGETRequestKey(HttpServletRequest request, HttpMessage message) {
		String key = "";
		if (null != request.getQueryString()) {
			key = key + "?" + request.getQueryString();
		}
		return key;
	}
	
	private static String md5(String cacheKey) {
		try {
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
		    digest.update(cacheKey.getBytes());
		    return new String(digest.digest());
		}
		catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
	
}