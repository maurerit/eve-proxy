package com.tlabs.eve.proxy;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    private Main() {
    }

    public static void main(String[] args) {
        final ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext("META-INF/context.xml");
        appContext.registerShutdownHook();
        appContext.start();
    }
}
